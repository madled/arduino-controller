/*
 Name:		ledcube_16.ino
 Created:	1/11/2022 9:24:44 PM
 Author:	Amber Albers
*/

#include <Wire.h>

#define SLAVE_ADDRESS (uint8_t)4

/**
 * PORT A, C : tristate led drivers
 *
 * PORT L : serial output
 *
 * PORT B 0 : serial clk
 * PORT B 1 : storage clk
 */

 // Tristate output bytes
#define TRISTATE_A_MODE DDRA
#define TRISTATE_A_DATA PORTA
#define TRISTATE_B_MODE DDRC
#define TRISTATE_B_DATA PORTC

// Serial data byte
#define SERIAL_DATA_MODE DDRL
#define SERIAL_DATA_DATA PORTL

// Serial control byte
#define SERIAL_CONTROL_MODE DDRB
#define SERIAL_CONTROl_DATA PORTB
#define SR_CLK (uint8_t)0 // Bit in port B
#define ST_CLK (uint8_t)1 // Bit in port B

// led data storage 
volatile uint8_t data[32][16];

void receiveData(int byteCount) {
  if (byteCount != 17) return;
  
  /**
   * Byte: |   16..1   |     0    |
   * Data: | LayerData | ColumnID |
   */

  uint8_t column = Wire.read();

  for (uint8_t i = 0; i < 16; i++)
    data[column][i] = Wire.read();
}

void setup() {
  // Disable pull-up resistors
  MCUCR |= _BV(PUD);

  // Set serial data byte to pinmode output
  SERIAL_DATA_MODE = 0xFF;

  // Set serial clock bits to pinmode output
  SERIAL_CONTROL_MODE |= _BV(SR_CLK);
  SERIAL_CONTROL_MODE |= _BV(ST_CLK);

  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(receiveData);
}

void loop() {

  /**
   * GPIOR0
   * Bit: |  7..5  |    4    |  3..1  |     0    |
   *      | Unused | A|B Reg | Column | Polarity |
   *
   * GPIOR1
   * Bit: |  7..4  |  3..0 |
   *      | Unused | Layer |
   */

   // Shift out data
  for (GPIOR1 = 0; GPIOR1 < 16; GPIOR1++) {
    // Serial data
    SERIAL_DATA_DATA = data[GPIOR0][GPIOR1];
    // Serial clock
    SERIAL_CONTROl_DATA |= _BV(SR_CLK);
    SERIAL_CONTROl_DATA &= ~_BV(SR_CLK);
  }

  noInterrupts();

  // Set tristate to pinmode input
  TRISTATE_A_MODE = 0;
  TRISTATE_B_MODE = 0;

  // Storage clock
  SERIAL_CONTROl_DATA |= _BV(ST_CLK);
  SERIAL_CONTROl_DATA &= ~_BV(ST_CLK);

  // Set tristate polarity
  TRISTATE_A_DATA = TRISTATE_B_DATA = (GPIOR0 & 0x1) * 0xFF;

  // Set tristate to pinmode output
  (GPIOR0 & 0x10 ? TRISTATE_B_MODE : TRISTATE_A_MODE) =
    1 << ((GPIOR0 & 0x0E) >> 1);

  interrupts();

  // Increment frame counter
  GPIOR0++;
}
